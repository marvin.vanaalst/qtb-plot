mkdocs >= 1.3.0
mkdocs-jupyter >= 0.21.0
mkdocs-material >= 8.2.15
notebook == 6.5.6
jupyter_contrib_nbextensions == 0.7.0
